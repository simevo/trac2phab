# trac2phab

quick'n'dirty python3 script to migrate from [trac](https://trac.edgewall.org/) to [phabricator](https://phacility.com/phabricator/)

trac tickets will be turned into Phabricator Maniphest Tasks

limitations:
- will skip authorship
- no attachments migration
- timestamps for trac tickets creation / modification will be lost
- in Phabricator, subprojects have their own separate workboards (tasks belonging to a subproject will only appear in the subproject workboard, not in the parent project's one); for this reason we found more fitting for us to use project as tags, having `XXX` as the main project then `XXX 1.1`, `XXX 1.2` ... for the milestones and `XXX user interface`, `XXX resolution` ... to encode the trac components
3. there is no equivalent of trac's severity and type fields, we have added them in the header row

how to use:

0. install prerequisites:

        sudo apt install python3-phabricator

1. create the main project where to put all the tasks that will be created (for us: `XXX`)

2. gather your main project, milestones and tags id from https://phabricator.example.com/conduit/method/project.query/, and paste their `PHID-...` in the configuration section of the script

3. run the script with `dry_run` set to `True`, examine the output (the script assumes the trac sqlite database file `trac.db` is the working directory)

4. you may want to adapt the script to yoru needs, in particular the cleanup function

5. then set `dry_run` to `False` and run it against your phabricator instance:

        ./trac2phab.py &> log

6. if the results disappoint you, prune all the tasks it has created with this shell oneliner (**caution**: use the task id range that applies to you !):

        for i in $(seq 607 1064); do ./phabricator/bin/remove destroy --force "T$i"; done
