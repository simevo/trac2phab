#!/usr/bin/env python3
# coding=utf-8

# ***** BEGIN LICENSE BLOCK *****
# trac2phab - quick'n'dirty script tp migrate from trac to phabricator
# homepage: https://gitlab.com/simevo/trac2phab
#
# license: GPL3+
# (C) Copyright 2019 Paolo Greppi simevo s.r.l.
# ***** END LICENSE BLOCK ***** */

import sqlite3
from phabricator import Phabricator
import json
import re

trac = re.compile(r'https:\/\/old.example.com\/trac\/ticket\/\d+')
trac1 = re.compile(r'#\d+')


def cleanup(s):
    s = s.replace('{{{', '```')
    s = s.replace('}}}', '```')

    # https://old.example.com/trac/ticket/283 -> https://phabricator.example.com/T888
    matches = trac.findall(s)
    for match in matches:
        trac_id = re.search(r'\d+', match).group()
        phab_id = int(trac_id) + offset
        s = s.replace('https://old.example.com/trac/ticket/%s' % trac_id, 'https://phabricator.example.com/T%d' % phab_id)

    # #111 -> [#111 -> T716](https://phabricator.example.com/T7126)
    matches = trac1.findall(s)
    for match in matches:
        trac_id = re.search(r'\d+', match).group()
        phab_id = int(trac_id) + offset
        s = s.replace(match, '[%s i.e. T%d](https://phabricator.example.com/T%d)' % (match, phab_id, phab_id))

    return s


db_file = 'trac.db'
connection = sqlite3.connect(db_file)
cursor = connection.cursor()

phab = Phabricator()  # This will use your ~/.arcrc file
print(phab.user.whoami())

# configuration start =========================================================
# dictionary to match workboard column names to PHID-PCOL-...
columns = {
  'Backlog': 'PHID-PCOL-uodg4rnporu5djpkpeqr',
  'todo': 'PHID-PCOL-65qbfx6efcben664d6ei',
  'doing': 'PHID-PCOL-qlgexiqah76urfouv752'
}
# dictionary to match project names to PHID-PROJ-...
projects = {
  'documentation': 'PHID-PROJ-tmpkj5kmm2yiofn33kb7',
  'interfaces': 'PHID-PROJ-ivt4iuerg27c2pmt2idh',
  'modeling': 'PHID-PROJ-ouf3xrnbzmzzpqtu6ybf',
  'opc configurator': 'PHID-PROJ-j4tlqzrwb2hjxujfwdkl',
  'physical properties': 'PHID-PROJ-cbyldwo5w5qdafkexm5w',
  'resolution': 'PHID-PROJ-oyoffkwgn3tmqsacyjdh',
  'systems': 'PHID-PROJ-ewkbunu44qktgzdcswzt',
  'user interface': 'PHID-PROJ-v7w4pw3yo7w7opilrstf'
}
milestones = {
  '2.0': 'PHID-PROJ-qkrp24yagza5wvd7yrl3',
  '1.3': 'PHID-PROJ-ztftef4hr3fjfct7oizo',
  '1.2': 'PHID-PROJ-moen57r44twzmpz32goz',
  '1.1': 'PHID-PROJ-b2wovhrgqo5kiwutls5n'
}
main_project = 'PHID-PROJ-esr5flit7uso2mlqyzfl'
offset = 604  # max Task ID before starting the migration
dry_run = True
# configuration end ===========================================================

query = "SELECT id, time, type, component, severity, priority, milestone, status, summary, description FROM ticket ORDER BY id ASC"
#               0   1     2     3          4         5         6          7       8        9
tickets = cursor.execute(query).fetchall()
for ticket in tickets:
    print(ticket)
    header = "imported from trac ticket **#%d** of type [**%s**] with severity = [**%s**]\n\n" % (ticket[0], ticket[2], ticket[4])
    transaction_list = [
      {
        "type": "title",
        "value": ticket[8]
      },
      {
        "type": "view",
        "value": main_project
      },
      {
        "type": "edit",
        "value": main_project
      }
    ]
    if ticket[6] == '1.0':
        transaction_list.append({
          "type": "projects.set",
          "value": [main_project, projects[ticket[3]]]
        })
    else:
        transaction_list.append({
          "type": "projects.set",
          "value": [main_project, projects[ticket[3]], milestones[ticket[6]]]
        })
    if ticket[9]:
        description = ticket[9]
        description = cleanup(description)
        transaction_list.append({
          "type": "description",
          "value": header + description
        })
    else:
        transaction_list.append({
          "type": "description",
          "value": header
        })
    if ticket[7] == 'closed':
        transaction_list.append({
          "type": "status",
          "value": 'resolved'
        })
    else:
        transaction_list.append({
          "type": "status",
          "value": 'open'
        })
        if ticket[7] == 'new' or ticket[7] == 'reopened':
            transaction_list.append({
              "type": "column",
              "value": [columns['Backlog']]
            })
        elif ticket[7] == 'accepted':
            transaction_list.append({
              "type": "column",
              "value": [columns['todo']]
            })
        elif ticket[7] == 'assigned':
            transaction_list.append({
              "type": "column",
              "value": [columns['doing']]
            })
    # major->normal ->low
    if ticket[5] == 'blocker' or ticket[5] == 'critical':
        transaction_list.append({
          "type": "priority",
          "value": 'high'
        })
    elif ticket[5] == 'minor' or ticket[5] == 'trivial':
        transaction_list.append({
          "type": "priority",
          "value": 'low'
        })
    elif ticket[5] == 'major':
        transaction_list.append({
          "type": "priority",
          "value": 'normal'
        })
    print(json.dumps(transaction_list, indent=4, sort_keys=True))
    if not dry_run:
        result = phab.maniphest.edit(transactions=transaction_list)
        print("created task %s" % result)
        ticket_id = result['object']['phid']
    query = "SELECT newvalue FROM ticket_change WHERE ticket=%d AND field = 'comment' ORDER BY time ASC" % ticket[0]
    changes = cursor.execute(query).fetchall()
    for change in changes:
        description = cleanup(change[0])
        transaction_list = [
          {
            'type': 'comment',
            'value': description
          }
        ]
        print(json.dumps(transaction_list, indent=4, sort_keys=True))
        if not dry_run:
            result = phab.maniphest.edit(transactions=transaction_list, objectIdentifier=ticket_id)
            print("added comment %s" % result)
